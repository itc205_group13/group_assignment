import java.util.Scanner;

public class BorrowBookUI {


    public enum UIState {
        INITIALISED,
        READY,
        RESTRICTED,
        SCANNING,
        FINALISING,
        COMPLETED,
        CANCELLED
    }

    private BorrowBookControl borrowBook;
    private Scanner scannerInput;
    private UIState state;


    public BorrowBookUI(BorrowBookControl commandHandler) {
        this.borrowBook = commandHandler;
        scannerInput = new Scanner(System.in);
        state = UIState.INITIALISED;
        borrowBook.setUI(this);
    }


    private String showPromptAndGetInput(String prompt) {
        System.out.print(prompt);
        return scannerInput.nextLine();
    }


    public void outputAdvisory(String advisory) {
        System.out.println(advisory);
    }


    public void setState(UIState state) {
        this.state = state;
    }


    public void run() {
        outputAdvisory("Welcome to the loans desk!\n");
        while (true) {
            switch (state) {
                case CANCELLED: {
                    outputAdvisory("Borrowing Cancelled\n");
                    return;
                }
                case READY: {
                    String memberIdInput = showPromptAndGetInput("Swipe member card (press <enter> to cancel): ");
                    if (memberIdInput.length() == 0) {
                        borrowBook.cancelBorrowing();
                        break;
                    }
                    try {
                        int memberId = Integer.valueOf(memberIdInput).intValue();
                        borrowBook.allowBorrowIfMember(memberId);
                    } catch (NumberFormatException exception) {
                        outputAdvisory("Invalid Member Id\n");
                    }
                    break;
                }
                case RESTRICTED: {
                    showPromptAndGetInput("Press <any key> to cancel");
                    borrowBook.cancelBorrowing();
                    break;
                }
                case SCANNING: {
                    String bookIdInput = showPromptAndGetInput("Scan Book (<enter> completes): ");
                    if (bookIdInput.length() == 0) {
                        borrowBook.complete();
                        break;
                    }
                    try {
                        int bookId = Integer.valueOf(bookIdInput).intValue();
                        borrowBook.scanned(bookId);
                    } catch (NumberFormatException exception) {
                        outputAdvisory("Invalid Book Id\n");
                    }
                    break;
                }
                case FINALISING: {
                    String confirmCommitInput = showPromptAndGetInput("Commit loans? (Y/N): ");
                    if (confirmCommitInput.toUpperCase().equals("Y")) {
                        showPromptAndGetInput("Press <any key> to complete ");
                        borrowBook.commitLoans();
                    } else {
                        borrowBook.cancelBorrowing();
                    }
                    break;
                }
                case COMPLETED: {
                    outputAdvisory("Borrowing Completed\n");
                    return;
                }
                default: {
                    outputAdvisory("Unhandled state");
                    throw new RuntimeException("BorrowBookUI : unhandled state :" + state);
                }
            }
        }
    }
}
