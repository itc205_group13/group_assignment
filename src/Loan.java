import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressWarnings("serial")
public class Loan implements Serializable {

    public enum LoanState {
        CURRENT,
        OVER_DUE,
        DISCHARGED
    }


    private int identifier;
    private Book bookObject;
    private Member memberObject;
    private Date dueDateObject;
    private LoanState state;


    public Loan(int loanId, Book currentBook, Member currentMember, Date dueDate) {
        this.identifier = loanId;
        this.bookObject = currentBook;
        this.memberObject = currentMember;
        this.dueDateObject = dueDate;
        this.state = LoanState.CURRENT;
    }


    public void checkOverDue() {
        if (state == LoanState.CURRENT &&
                Calendar.getInstance().currentDate().after(dueDateObject)) {
            this.state = LoanState.OVER_DUE;
        }
    }


    public boolean isOverDue() {
        return state == LoanState.OVER_DUE;
    }


    public int getId() {
        return identifier;
    }


    public Date getDueDate() {
        return dueDateObject;
    }


    public Member getMember() {
        return memberObject;
    }


    public Book getBook() {
        return bookObject;
    }


    public void loanDischarge() {
        state = LoanState.DISCHARGED;
    }


    public String toString() {
        SimpleDateFormat shortDate = new SimpleDateFormat("dd/MM/yyyy");
        StringBuilder output = new StringBuilder();
        int outputID = memberObject.getId();
        String outputLastName = memberObject.getLastName();
        String outputFirstName = memberObject.getFirstName();
        int bookID = bookObject.getId();
        String bookTitle = bookObject.getTitle();
        String dueDate = shortDate.format(dueDateObject);
        output.append("Loan:  ").append(identifier).append("\n")
                .append("  Borrower ").append(outputID).append(" : ")
                .append(outputLastName).append(", ").append(outputFirstName).append("\n")
                .append("  Book ").append(bookID).append(" : " )
                .append(bookTitle).append("\n")
                .append("  DueDate: ").append(dueDate).append("\n")
                .append("  State: ").append(state);
        return output.toString();
    }
}
