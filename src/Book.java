import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {

    
    private enum BookState {
        AVAILABLE,
        ON_LOAN,
        DAMAGED
    }

    private BookState state;
    private String title;
    private String author;
    private String callNumber;
    private int identifier;


    public Book(String bookAuthor, String bookTitle, String bookCallNumber, int bookId) {
        author = bookAuthor;
        title = bookTitle;
        callNumber = bookCallNumber;
        identifier = bookId;
        state = BookState.AVAILABLE;
    }


    public String toString() {
        StringBuilder bookInformation = new StringBuilder();
        bookInformation.append("Book: ")
                        .append(identifier)
                        .append("\n")
                        .append("  Title:  ")
                        .append(title)
                        .append("\n")
                        .append("  Author: ")
                        .append(author)
                        .append("\n")
                        .append("  CallNo: ")
                        .append(callNumber)
                        .append("\n")
                        .append("  State:  ")
                        .append(state);

        return bookInformation.toString();
    }


    public Integer getId() {
        return identifier;
    }


    public String getTitle() {
        return title;
    }


    public boolean isAvailable() {
        return state == BookState.AVAILABLE;
    }


    public boolean isLoaned() {
        return state == BookState.ON_LOAN;
    }


    public boolean isDamaged() {
        return state == BookState.DAMAGED;
    }


    public void borrowBook() {
        if (state.equals(BookState.AVAILABLE)) {
            state = BookState.ON_LOAN;
        } else {
            String errorMessage = String.format("Book: cannot borrow while book is in state: %s", state);
            throw new RuntimeException(errorMessage);
        }
    }


    public void returnBook(boolean isBookDamaged) {
        if (state.equals(BookState.ON_LOAN)) {
            if (isBookDamaged) {
                state = BookState.DAMAGED;
            } else {
                state = BookState.AVAILABLE;
            }
        } else {
            String errorMessage = String.format("Book: cannot Return while book is in state: %s", state);
            throw new RuntimeException(errorMessage);
        }
    }


    public void repairBook() {
        if (state.equals(BookState.DAMAGED)) {
            state = BookState.AVAILABLE;
        } else {
            String errorMessage = String.format("Book: cannot repair while book is in state: %s", state);
            throw new RuntimeException(errorMessage);
        }
    }
}
