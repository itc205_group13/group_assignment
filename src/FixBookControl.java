public class FixBookControl {


    private enum ControlState {
        INITIALISED,
        READY,
        FIXING
    }


    private FixBookUI ui;
    private ControlState state;
    private Library currentLibrary;
    private Book currentBook;



    public FixBookControl() {
        currentLibrary = Library.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUI(FixBookUI ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
        }
        this.ui = ui;
        ui.setState(FixBookUI.UIState.READY);
        state = ControlState.READY;
    }


    public void bookScanned(int bookId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
        }
        currentBook = currentLibrary.getBook(bookId);

        if (currentBook == null) {
            ui.display("Invalid bookId");
            return;
        }
        if (!currentBook.isDamaged()) {
            ui.display("\"Book has not been damaged");
            return;
        }
        String bookID = currentBook.toString();
        ui.display(bookID);
        ui.setState(FixBookUI.UIState.FIXING);
        state = ControlState.FIXING;
    }


    public void fixBook(boolean fix) {
        if (!state.equals(ControlState.FIXING)) {
            throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
        }
        if (fix) {
            currentLibrary.repairBook(currentBook);
        }
        currentBook = null;
        ui.setState(FixBookUI.UIState.READY);
        state = ControlState.READY;
    }


    public void scanningComplete() {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
        }
        ui.setState(FixBookUI.UIState.COMPLETED);
    }
}
