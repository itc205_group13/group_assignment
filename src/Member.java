import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SuppressWarnings("serial")
public class Member implements Serializable {


    private String lastName;
    private String firstName;
    private String email;
    private int phoneNumber;
    private int identifier;
    private double totalFines;
    private Map<Integer, Loan> currentLoans;


    public Member(String memberLastName, String memberFirstName, String memberEmail,
                  int memberPhoneNumber, int memberId) {
        lastName = memberLastName;
        firstName = memberFirstName;
        email = memberEmail;
        phoneNumber = memberPhoneNumber;
        identifier = memberId;
        currentLoans = new HashMap<>();
    }


    public String toString() {
        StringBuilder memberInformation = new StringBuilder();
        String finesInformation = String.format("  Fines Owed :  $%.2f", totalFines);
        memberInformation.append("Member:  ")
                            .append(identifier)
                            .append("\n")
                            .append("  Name:  ")
                            .append(lastName)
                            .append(", ")
                            .append(firstName)
                            .append("\n")
                            .append("  Email: ")
                            .append(email)
                            .append("\n")
                            .append("  Phone: ")
                            .append(phoneNumber)
                            .append("\n")
                            .append(finesInformation)
                            .append("\n");

        for (Loan loan : currentLoans.values()) {
            memberInformation.append(loan).append("\n");
        }
        return memberInformation.toString();
    }


    public int getId() {
        return identifier;
    }


    public List<Loan> getLoans() {
        return new ArrayList<Loan>(currentLoans.values());
    }


    public int getNumberOfCurrentLoans() {
        return currentLoans.size();
    }


    public double getFinesOwed() {
        return totalFines;
    }


    public void takeOutLoan(Loan newLoan) {
        int loanId = newLoan.getId();
        if (!currentLoans.containsKey(loanId)) {
            currentLoans.put(loanId, newLoan);
        } else {
            throw new RuntimeException("Duplicate loan added to member");
        }
    }

    
    public String getLastName() {
        return lastName;
    }


    public String getFirstName() {
        return firstName;
    }


    public void addFine(double newFine) {
        totalFines += newFine;
    }


    public double payFine(double amountPaid) {
        if (amountPaid < 0) {
            throw new RuntimeException("Member.payFine: amount must be positive");
        }
        double change = 0;
        if (amountPaid > totalFines) {
            change = amountPaid - totalFines;
            totalFines = 0;
        } else {
            totalFines -= amountPaid;
        }
        return change;
    }


    public void dischargeLoan(Loan loan) {
        int loanId = loan.getId();
        if (currentLoans.containsKey(loanId)) {
            currentLoans.remove(loanId);
        } else {
            throw new RuntimeException("No such loan held by member");
        }
    }
}
