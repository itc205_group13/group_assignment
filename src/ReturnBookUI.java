import java.util.Scanner;


public class ReturnBookUI {


    public enum UIState {
        INITIALISED,
        READY,
        INSPECTING,
        COMPLETED
    }
    private ReturnBookControl control;
    private Scanner userInput;
    private UIState state;


    public ReturnBookUI(ReturnBookControl returnBookControl) {
        control = returnBookControl;
        userInput = new Scanner(System.in);
        state = UIState.INITIALISED;
        control.setUI(this);
    }


    public void run() {
        output("Return Book Use Case UI\n");

        while (true) {

            switch (state) {
                case INITIALISED: {
                    break;
                }
                case READY: {
                    String bookIdInput = getUserInput("Scan Book (<enter> completes): ");
                    if (bookIdInput.length() == 0) {
                      control.scanningComplete();
                    } else {
                        try {
                        int bookId = Integer.valueOf(bookIdInput).intValue();
                        control.bookScanned(bookId);
                        } catch (NumberFormatException exception) {
                            output("Invalid bookId");
                        }
                    }
                    break;
                }
                case INSPECTING: {
                    String bookDamagedInput = getUserInput("Is book damaged? (Y/N): ");
                    boolean isBookDamaged = false;
                    if (bookDamagedInput.toUpperCase().equals("Y")) {
                            isBookDamaged = true;
                    }
                    control.dischargeLoan(isBookDamaged);
                    // fall through
                }
                case COMPLETED: {
                    output("Return processing complete");
                    return;
                }
                default: {
                    output("Unhandled state");
                    throw new RuntimeException("ReturnBookUI : unhandled state :" + state);
                }
            }
        }
    }


    private String getUserInput(String prompt) {
        System.out.print(prompt);
        return userInput.nextLine();
    }


    private void output(Object toPrint) {
        System.out.println(toPrint);
    }


    public void display(Object toDisplay) {
        output(toDisplay);
    }


    public void setState(UIState newState) {
        state = newState;
    }
}
