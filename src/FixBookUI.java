import java.util.Scanner;


public class FixBookUI {


    public enum UIState {
        INITIALISED,
        READY,
        FIXING,
        COMPLETED
    }


    private FixBookControl fixControl;
    private Scanner input;
    private UIState state;


    public FixBookUI(FixBookControl control) {
        this.fixControl = control;
        input = new Scanner(System.in);
        state = UIState.INITIALISED;
        fixControl.setUI(this);
    }


    public void setState(UIState state) {
        this.state = state;
    }


    private String getInputAndProcess(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }


    public void display(Object message) {
        outputNotice(message);
    }


    private void outputNotice(Object message) {
        System.out.println(message);
    }


    public void run() {
        outputNotice("Fix Book Use Case UI\n");
        while (true) {
            switch (state) {
                case READY: {
                    String bookID = getInputAndProcess("Scan Book (<enter> completes): ");
                    if (bookID.length() == 0) {
                        fixControl.scanningComplete();
                    } else {
                        try {
                            int bookId = Integer.valueOf(bookID).intValue();
                            fixControl.bookScanned(bookId);
                        } catch (NumberFormatException e) {
                            outputNotice("Invalid bookId");
                        }
                    }
                    break;
                }
                case FIXING: {
                    String whetherFix = getInputAndProcess("Fix Book? (Y/N) : ");
                    boolean fix = false;
                    if (whetherFix.toUpperCase().equals("Y")) {
                        fix = true;
                    }
                    fixControl.fixBook(fix);
                    break;
                }
                case COMPLETED: {
                    outputNotice("Fixing process complete");
                    return;
                }
                default: {
                    outputNotice("Unhandled state");
                    throw new RuntimeException("FixBookUI : unhandled state :" + state);
                }
            }
        }
    }
}
