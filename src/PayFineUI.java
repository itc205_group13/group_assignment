import java.util.Scanner;


public class PayFineUI {


    public enum UIState {
        INITIALISED,
        READY,
        PAYING,
        COMPLETED,
        CANCELLED
    }


    private PayFineControl fineControl;
    private Scanner input;
    private UIState state;


    public PayFineUI(PayFineControl control) {
        this.fineControl = control;
        input = new Scanner(System.in);
        state = UIState.INITIALISED;
        fineControl.setUI(this);
    }


    private String requestInputAndProcess(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }


    public void display(Object message) {
        outputNotice(message);
    }


    public void setState(UIState state) {
        this.state = state;
    }


    private void outputNotice(Object message) {
        System.out.println(message);
    }


    public void run() {
        outputNotice("Pay Fine Use Case UI\n");

        while (true) {
            switch (state) {
                case READY: {
                    String memberCard = requestInputAndProcess("Swipe member card (press <enter> to cancel): ");
                    if (memberCard.length() == 0) {
                        fineControl.cancel();
                        break;
                    }
                    try {
                        int memberId = Integer.valueOf(memberCard).intValue();
                        fineControl.cardSwiped(memberId);
                    } catch (NumberFormatException exception) {
                        outputNotice("Invalid memberId");
                    }
                    break;
                }

                case PAYING: {
                    double amount = 0;
                    String fineAmount = requestInputAndProcess("Enter amount (<Enter> cancels) : ");
                    if (fineAmount.length() == 0) {
                        fineControl.cancel();
                        break;
                    }
                    try {
                        amount = Double.valueOf(fineAmount).doubleValue();
                    } catch (NumberFormatException exception) {
                        // it's not numeric; that's fine, just continue
                    }
                    if (amount <= 0) {
                        outputNotice("Amount must be positive");
                        break;
                    }
                    fineControl.payFine(amount);
                    break;
                }

                case CANCELLED: {
                    outputNotice("Pay Fine process cancelled");
                    return;
                }

                case COMPLETED: {
                    outputNotice("Pay Fine process complete");
                    return;
                }

                default: {
                    outputNotice("Unhandled state");
                    throw new RuntimeException("PayFineUI : unhandled state :" + state);
                }
            }
        }
    }
}
