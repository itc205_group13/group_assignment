import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {

    
    private static final String LIBRARY_FILE = "library.obj";
    private static final int LOAN_LIMIT = 2;
    private static final int LOAN_PERIOD = 2;
    private static final double FINE_PER_DAY = 1.0;
    private static final double MAX_FINES_OWED = 5.0;
    private static final double DAMAGE_FEE = 2.0;
    private static Library self;
    private int bookId;
    private int merchantId;
    private int loanId;
    private Date loadDate;
    private Map<Integer, Book> catalog;
    private Map<Integer, Member> members;
    private Map<Integer, Loan> loans;
    private Map<Integer, Loan> borrowedBooks;
    private Map<Integer, Book> damagedBooks;


    private Library() {
        catalog = new HashMap<>();
        members = new HashMap<>();
        loans = new HashMap<>();
        borrowedBooks = new HashMap<>();
        damagedBooks = new HashMap<>();
        bookId = 1;
        merchantId = 1;
        loanId = 1;
    }


    public static synchronized Library getInstance() {
        if (self == null) {
            Path path = Paths.get(LIBRARY_FILE);
            if (Files.exists(path)) {
                try (ObjectInputStream libraryOutputFile = new ObjectInputStream(new FileInputStream(LIBRARY_FILE));) {
                    self = (Library) libraryOutputFile.readObject();
                    Calendar.getInstance().setDate(self.loadDate);
                    libraryOutputFile.close();
                } catch(Exception exception) {
                    throw new RuntimeException(exception);
                }
            } else {
                self = new Library();
            }
        }
        return self;
    }


    public static synchronized void saveLibrary() {
        if (self != null) {
            self.loadDate = Calendar.getInstance().currentDate();
            try (ObjectOutputStream libraryOutputFile = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) {
                libraryOutputFile.writeObject(self);
                libraryOutputFile.flush();
                libraryOutputFile.close();
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        }
    }


    private int newBookId() {
        return bookId++;
    }


    private int newMemberId() {
        return merchantId++;
    }


    private int newLoanId() {
        return loanId++;
    }


    public List<Member> listMembers() {
        return new ArrayList<Member>(members.values());
    }


    public List<Book> listBooks() {
        return new ArrayList<Book>(catalog.values());
    }


    public List<Loan> currentLoans() {
        return new ArrayList<Loan>(borrowedBooks.values());
    }


    public Member addMember(String lastName, String firstName, String email, int phoneNumber) {
        int memberId = newMemberId();
        Member member = new Member(lastName, firstName, email, phoneNumber, memberId);
        members.put(memberId, member);
        return member;
    }


    public Book addBook(String author, String title, String callNumber) {
        int newBookId = newBookId();
        Book newBook = new Book(author, title, callNumber, newBookId);
        catalog.put(newBookId, newBook);
        return newBook;
    }


    public Member getMember(int memberId) {
        if (members.containsKey(memberId)) {
            return members.get(memberId);
        }
        return null;
    }


    public Book getBook(int bookId) {
        if (catalog.containsKey(bookId)) {
            return catalog.get(bookId);
        }
        return null;
    }


    public boolean memberCanBorrow(Member memberBorrow) {
        if (memberBorrow.getNumberOfCurrentLoans() == LOAN_LIMIT) {
            return false;
        }
        if (memberBorrow.getFinesOwed() >= MAX_FINES_OWED) {
            return false;
        }
        for (Loan loan : memberBorrow.getLoans()) {
            if (loan.isOverDue()) {
                return false;
            }
        }
        return true;
    }


    public int loansRemainingForMember(Member memberLoans) {
        return LOAN_LIMIT - memberLoans.getNumberOfCurrentLoans();
    }


    public Loan issueLoan(Book bookLoan, Member memberLoan) {
        int loanId = newLoanId();
        int bookId = bookLoan.getId();
        Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD);
        Loan loan = new Loan(loanId, bookLoan, memberLoan, dueDate);
        memberLoan.takeOutLoan(loan);
        bookLoan.borrowBook();
        loans.put(loanId, loan);
        borrowedBooks.put(bookId, loan);
        return loan;
    }


    public Loan getLoanByBookId(int bookId) {
        if (borrowedBooks.containsKey(bookId)) {
            return borrowedBooks.get(bookId);
        }
        return null;
    }


    public double calculateOverDueFine(Loan loanDue) {
        Date overdueLoan = loanDue.getDueDate();
        if (loanDue.isOverDue()) {
            long daysOverDue = Calendar.getInstance().getDaysDifference(overdueLoan);
            double fine = daysOverDue * FINE_PER_DAY;
            return fine;
        }
        return 0.0;
    }


    public void dischargeLoan(Loan currentLoan, boolean isBookDamaged) {
        Member memberDischarge = currentLoan.getMember();
        Book bookDischarge  = currentLoan.getBook();
        int bookId = bookDischarge.getId();
        double overDueFine = calculateOverDueFine(currentLoan);
        memberDischarge.addFine(overDueFine);
        memberDischarge.dischargeLoan(currentLoan);
        bookDischarge.returnBook(isBookDamaged);
        if (isBookDamaged) {
            memberDischarge.addFine(DAMAGE_FEE);
            damagedBooks.put(bookId, bookDischarge);
        }
        currentLoan.loanDischarge();
        borrowedBooks.remove(bookId);
    }


    public void checkCurrentLoans() {
        for (Loan loan : borrowedBooks.values()) {
            loan.checkOverDue();
        }
    }


    public void repairBook(Book currentBook) {
        int bookId = currentBook.getId();
        if (damagedBooks.containsKey(bookId)) {
            currentBook.repairBook();
            damagedBooks.remove(bookId);
        } else {
            throw new RuntimeException("Library: repairBook: book is not damaged");
        }
    }
}
