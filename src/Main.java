import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class Main {


    private static Scanner userInput;
    private static Library library;
    private static String menu;
    private static Calendar calendar;
    private static SimpleDateFormat dateFormatter;


    private static String getMenu() {
        StringBuilder menuText = new StringBuilder();
        menuText.append("\nLibrary Main Menu\n\n")
                .append("  M  : add member\n")
                .append("  LM : list members\n")
                .append("\n")
                .append("  B  : add book\n")
                .append("  LB : list books\n")
                .append("  FB : fix books\n")
                .append("\n")
                .append("  L  : take out a loan\n")
                .append("  R  : return a loan\n")
                .append("  LL : list loans\n")
                .append("\n")
                .append("  P  : pay fine\n")
                .append("\n")
                .append("  T  : increment date\n")
                .append("  Q  : quit\n")
                .append("\n")
                .append("Choice : ");
        return menuText.toString();
    }


    public static void main(String[] args) {
        try {
            userInput = new Scanner(System.in);
            library = Library.getInstance();
            calendar = Calendar.getInstance();
            dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

            for (Member currentMember : library.listMembers()) {
                output(currentMember);
            }
            output(" ");
            for (Book currentBook : library.listBooks()) {
                output(currentBook);
            }

            menu = getMenu();

            boolean terminateFlag = false;

            while (!terminateFlag) {
                Date currentDateAndTime = calendar.currentDate();
                String currentDate = dateFormatter.format(currentDateAndTime);
                output("\n" + currentDate);
                String userMenuChoice = getUserInput(menu);

                switch (userMenuChoice.toUpperCase()) {
                    case "M": {
                        addMember();
                        break;
                    }
                    case "LM": {
                        listMembers();
                        break;
                    }
                    case "B": {
                        addBook();
                        break;
                    }
                    case "LB": {
                        listBooks();
                        break;
                    }
                    case "FB": {
                        fixBook();
                        break;
                    }
                    case "L": {
                        borrowBook();
                        break;
                    }
                    case "R": {
                        returnBook();
                        break;
                    }
                    case "LL": {
                        listCurrentLoans();
                        break;
                    }
                    case "P": {
                        payFine();
                        break;
                    }
                    case "T": {
                        incrementDate();
                        break;
                    }
                    case "Q": {
                        terminateFlag = true;
                        break;
                    }
                    default: {
                        output("\nInvalid option\n");
                        break;
                    }
                }
                Library.saveLibrary();
            }
        } catch (RuntimeException exception) {
            output(exception);
        }
        output("\nEnded\n");
    }


    private static void payFine() {
        PayFineControl payFineController = new PayFineControl();
        new PayFineUI(payFineController).run();
    }


    private static void listCurrentLoans() {
        output("");
        for (Loan currentLoan : library.currentLoans()) {
            output(currentLoan + "\n");
        }
    }


    private static void listBooks() {
        output("");
        for (Book book : library.listBooks()) {
            output(book + "\n");
        }
    }


    private static void listMembers() {
        output("");
        for (Member member : library.listMembers()) {
            output(member + "\n");
        }
    }


    private static void borrowBook() {
        BorrowBookControl borrowBookController = new BorrowBookControl();
        new BorrowBookUI(borrowBookController).run();
    }


    private static void returnBook() {
        ReturnBookControl returnBookController = new ReturnBookControl();
        new ReturnBookUI(returnBookController).run();
    }


    private static void fixBook() {
        FixBookControl fixBookController = new FixBookControl();
        new FixBookUI(fixBookController).run();
    }


    private static void incrementDate() {
        try {
            String userInputNumberOfDays = getUserInput("Enter number of days: ");
            int numberOfDays = Integer.valueOf(userInputNumberOfDays).intValue();
            calendar.incrementDate(numberOfDays);
            library.checkCurrentLoans();
            Date incrementedDateInformation = calendar.currentDate();
            String dateInformation = dateFormatter.format(incrementedDateInformation);
            output(dateInformation);
        } catch (NumberFormatException exception) {
            output("\nInvalid number of days\n");
        }
    }


    private static void addBook() {
        String userInputAuthor = getUserInput("Enter author: ");
        String userInputTitle = getUserInput("Enter title: ");
        String userInputCallNumber = getUserInput("Enter call number: ");
        Book newBook = library.addBook(userInputAuthor, userInputTitle, userInputCallNumber);
        output("\n" + newBook + "\n");
    }


    private static void addMember() {
        try {
            String userInputLastName = getUserInput("Enter last name: ");
            String userInputFirstName = getUserInput("Enter first name: ");
            String userInputEmail = getUserInput("Enter email: ");
            String userInputPhone = getUserInput("Enter phone number: ");
            int phoneNumber = Integer.valueOf(userInputPhone).intValue();
            Member newMember = library.addMember(userInputLastName, userInputFirstName, userInputEmail, phoneNumber);
            output("\n" + newMember + "\n");
        } catch (NumberFormatException exception) {
            output("\nInvalid phone number\n");
        }
    }


    private static String getUserInput(String prompt) {
        System.out.print(prompt);
        return userInput.nextLine();
    }


    private static void output(Object toPrint) {
        System.out.println(toPrint);
    }
}
