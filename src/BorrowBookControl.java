import java.util.ArrayList;
import java.util.List;

public class BorrowBookControl {


    private enum ControlState {
        INITIALISED,
        READY,
        SCANNING,
        FINALISING,
        COMPLETED,
        CANCELLED
    }

    private BorrowBookUI ui;
    private Library libraryArchive;
    private Member memberArchive;
    private ControlState state;
    private List<Book> availableBooks;
    private List<Loan> loanedBooks;
    private Book bookArchive;


    public BorrowBookControl() {
        this.libraryArchive = libraryArchive.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUI(BorrowBookUI ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException(this.getClass().getName() + ": cannot call" +
                    new Object() {}.getClass().getEnclosingMethod().getName() + "except in INITIALISED state");
        }
        this.ui = ui;
        ui.setState(BorrowBookUI.UIState.READY);
        state = ControlState.READY;
    }


    public void allowBorrowIfMember(int memberId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException(this.getClass().getName() + ": cannot call " +
                    new Object() {}.getClass().getEnclosingMethod().getName() + "except in READY state");
        }
        memberArchive = libraryArchive.getMember(memberId);
        if (memberArchive == null) {
            ui.outputAdvisory("Invalid Member Id\n");
            return;
        }
        if (!libraryArchive.memberCanBorrow(memberArchive)) {
            ui.outputAdvisory("Member cannot borrow at this time\n");
            ui.setState(BorrowBookUI.UIState.RESTRICTED);
            return;
        }
        availableBooks = new ArrayList<>();
        ui.setState(BorrowBookUI.UIState.SCANNING);
        state = ControlState.SCANNING;
    }


    public void scanned(int bookId) {
        bookArchive = null;
        if (!state.equals(ControlState.SCANNING)) {
            throw new RuntimeException(this.getClass().getName() + ": cannot call" +
                    new Object() {}.getClass().getEnclosingMethod().getName() + "except in SCANNING state");
        }
        bookArchive = libraryArchive.getBook(bookId);
        if (bookArchive == null) {
            ui.outputAdvisory("Invalid book Id\n");
            return;
        }
        if (!bookArchive.isAvailable()) {
            ui.outputAdvisory("Book cannot be borrowed\n");
            return;
        }
        availableBooks.add(bookArchive);
        for (Book book : availableBooks) {
            String showBook = book.toString();
            ui.outputAdvisory(showBook);
        }
        int maxBorrow = libraryArchive.loansRemainingForMember(memberArchive) - availableBooks.size();
        if (maxBorrow == 0 ) {
            ui.outputAdvisory("Loan limit reached\n");
            complete();
        }
    }


    public void complete() {
        if (availableBooks.size() == 0) {
            cancelBorrowing();
        } else {
            ui.outputAdvisory("Final Borrowing List\n");
        }
        for (Book book : availableBooks) {
            String showBook = book.toString();
            ui.outputAdvisory(showBook);
            loanedBooks = new ArrayList<>();
            ui.setState(BorrowBookUI.UIState.FINALISING);
            state = ControlState.FINALISING;
        }
    }


    public void commitLoans() {
        if (!state.equals(ControlState.FINALISING)) {
            throw new RuntimeException(this.getClass().getName() + ": cannot call "  +
                    new Object() {}.getClass().getEnclosingMethod().getName() + " except in FINALISING state");
        }
        for (Book book : availableBooks) {
            Loan loan = libraryArchive.issueLoan(book, memberArchive);
            loanedBooks.add(loan);
        }
        ui.outputAdvisory("Completed Loan Slip\n");
        for (Loan loan : loanedBooks) {
            String showLoan = loan.toString();
            ui.outputAdvisory(showLoan);
            ui.setState(BorrowBookUI.UIState.COMPLETED);
            state = ControlState.COMPLETED;
        }
    }


    public void cancelBorrowing() {
        ui.setState(BorrowBookUI.UIState.CANCELLED);
        state = ControlState.CANCELLED;
    }
}
