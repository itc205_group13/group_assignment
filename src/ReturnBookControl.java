public class ReturnBookControl {


    private enum ControlState {
        INITIALISED,
        READY,
        INSPECTING
    }
    private ControlState state;
    private ReturnBookUI ui;
    private Library library;
    private Loan currentLoan;


    public ReturnBookControl() {
        this.library = library.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUI(ReturnBookUI returnBookInterface) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
        }
        ui = returnBookInterface;
        ui.setState(ReturnBookUI.UIState.READY);
        state = ControlState.READY;
    }


    public void bookScanned(int bookId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
        }
        Book scannedBook = library.getBook(bookId);
        if (scannedBook == null) {
            ui.display("Invalid Book Id");
            return;
        }
        if (!scannedBook.isLoaned()) {
            ui.display("Book has not been borrowed");
            return;
        }
        currentLoan = library.getLoanByBookId(bookId);
        double overDueFine = 0.0;
        if (currentLoan.isOverDue()) {
            overDueFine = library.calculateOverDueFine(currentLoan);
        }
        ui.display("Inspecting");
        String scannedBookMessage = scannedBook.toString();
        String currentLoanMessage = currentLoan.toString();
        ui.display(scannedBookMessage);
        ui.display(currentLoanMessage);
        if (currentLoan.isOverDue()) {
            String overdueMessage = String.format("\nOverdue fine : $%.2f", overDueFine);
            ui.display(overdueMessage);
        }
        ui.setState(ReturnBookUI.UIState.INSPECTING);
        state = ControlState.INSPECTING;
    }


    public void scanningComplete() {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
        }
        ui.setState(ReturnBookUI.UIState.COMPLETED);
    }


    public void dischargeLoan(boolean isBookDamaged) {
        if (!state.equals(ControlState.INSPECTING)) {
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
        }
        library.dischargeLoan(currentLoan, isBookDamaged);
        currentLoan = null;
        ui.setState(ReturnBookUI.UIState.READY);
        state = ControlState.READY;
    }
}
