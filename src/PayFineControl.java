public class PayFineControl {


    private enum ControlState {
        INITIALISED,
        READY,
        PAYING,
        COMPLETED,
        CANCELLED
    }

    private PayFineUI ui;
    private ControlState state;
    private Library library;
    private Member currentMember;;


    public PayFineControl() {
        this.library = library.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUI(PayFineUI ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
        }
        this.ui = ui;
        ui.setState(PayFineUI.UIState.READY);
        state = ControlState.READY;
    }


    public void cardSwiped(int memberId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
        }
        currentMember = library.getMember(memberId);

        if (currentMember == null) {
            ui.display("Invalid Member Id");
            return;
        }
        String memberCard = currentMember.toString();
        ui.display(memberCard);
        ui.setState(PayFineUI.UIState.PAYING);
        state = ControlState.PAYING;
    }


    public void cancel() {
        ui.setState(PayFineUI.UIState.CANCELLED);
        state = ControlState.CANCELLED;
    }


    public double payFine(double amount) {
        if (!state.equals(ControlState.PAYING)) {
            throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
        }
        double change = currentMember.payFine(amount);
        if (change > 0) {
            String changeToGive = String.format("Change: $%.2f", change);
            ui.display(changeToGive);
        }
        String finePayable = currentMember.toString();
        ui.display(finePayable);
        ui.setState(PayFineUI.UIState.COMPLETED);
        state = ControlState.COMPLETED;
        return change;
    }
}
